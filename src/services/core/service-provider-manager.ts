/* eslint-disable @typescript-eslint/ban-types */
import { ServiceProvider } from './service-provider';

/**
 * A class that allows registration and retrieval of services.
 */
export class ServiceProviderManager {
  private services: ServiceRegistration[] = [];

  /**
   * Register a service provider based on class name.
   * e.g FileSystem instance provided will be available as 'FileSystem' service.
   * @param service The instance to be used as service provider
   */
  register(service: ServiceProvider): void {
    service.provides().forEach((name) => {
      service.addToIocContainer(this);
      this.services.push({
        name: name,
        provider: service
      });
    });
  }

  /**
   * Unregister a service provider.
   * @param service The instance to be removed from service providers
   */
  unregister(service: ServiceProvider): void {
    service.provides().forEach((name) => {
      const index = this.services.findIndex((service) => service.name === name);
      if (index >= 0) {
        this.services.splice(index, 1);
      }
    });
  }

  /**
   * Returns the first available provider for a required service.
   * If generic type provided it will cast instance to provided type.
   * @param name The type of the provider (class).
   * @returns Service, or null if not found.
   */
  resolve<T extends ServiceProvider>(type: (Function & { prototype: T })): T | null {
    const service = this.services.find(service => service.name === type.name);
    if (service) {
      return <T>service.provider;
    } else {
      return null;
    }
  }

  /**
   * Return unique service names available
   */
  serviceNames(): string[] {
    const names = this.services.map((service) => service.name);
    return [...new Set(names)];
  }

  /**
   * Returns all providers for a specific service
   * @param type class type/name that provide specified service
   */
  providers<T extends ServiceProvider>(type?: Function): T[] {
    return this.services
      .filter(service => service.name === type?.name)
      .map(service => <T>(service.provider));
  }
}

interface ServiceRegistration {
  name: string;
  provider: ServiceProvider;
}
