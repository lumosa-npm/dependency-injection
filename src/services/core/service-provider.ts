import { EventEmitter } from 'events';
import { ServiceProviderManager } from './service-provider-manager';

/**
 * A interface that allows a class to be added a service provider
 */
export abstract class ServiceProvider extends EventEmitter {
  protected serviceManager: ServiceProviderManager = null;

  constructor() {
    super();
  }

  /**
   * Adds Service to required Inversion of Control container (ServiceProviderManager)
   * @param serviceManager 
   */
  addToIocContainer(serviceManager: ServiceProviderManager): void {
    if (serviceManager) {
      this.serviceManager = serviceManager;
    }
  }

  /**
   * The services that can provide
   */
  provides(): string[] {
    return this.findBaseClassNames(this);
  }

  /**
   * The name of the service
   * @returns class name string
   */
  name(): string {
    return this.constructor.name;
  }

  /**
   * Populate base classes to be register as a service provider.
   * loops through all super classes recursively.
   * @param service the
   */
  private findBaseClassNames(service: ServiceProvider): string[] {
    const blacklistNames = ['ServiceProvider', 'EventEmitter', 'Object', 'undefined', null];
    const names = [];

    let baseClass = service;
    while (baseClass) {
      const subClass = Object.getPrototypeOf(baseClass);
      if (!!subClass && !blacklistNames.includes(subClass.constructor.name)) {
        baseClass = subClass;
        names.push(subClass.constructor.name);
      } else {
        break;
      }
    }

    return names;
  }
}
