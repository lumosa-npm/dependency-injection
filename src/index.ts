/* eslint-disable no-console */
import { EventEmitter } from 'events';
import { EventName } from './models/di-events-model';
import { DependencyInjectionOptions } from './models/di-options-model';
import { ServiceProvider } from './services/core/service-provider';
import { ServiceProviderManager } from './services/core/service-provider-manager';
import { FileSystemService } from './services/file/file-system-service';

export class DependencyInjectionFramework extends EventEmitter {

  private _serviceManager: ServiceProviderManager = new ServiceProviderManager();
  private _excludePlugins: string[] = [];

  constructor() {
    super();
  }

  /**
   * Starts dependency injection framework
   * @param options options for dependency injection DependencyInjectionOptions
   */
  public start(options: DependencyInjectionOptions = { debugOn: true, loadPluginsFrom: [], excludePlugins: [] }): void {
    // Set exclude plugins to var
    this._excludePlugins = options.excludePlugins;

    // On BeforeLoad event
    this.on(EventName.BeforeLoad, async () => {
      // Register file system service
      this._serviceManager.register(new FileSystemService());

      // Load plugins
      await this.load(options.loadPluginsFrom);
    });

    // Emit BeforeLoad event
    this.emit(EventName.BeforeLoad);
  }

  /**
 * Retrieve service provide manager
 */
  serviceProviderManager(): ServiceProviderManager {
    return this._serviceManager;
  }

  /**
   * Load the service providers modules from file.
   * One needs to provide the list of folders where to look for plugins. If no folder provider,
   * it will search by default in ./plugins folder
   * @param folders a list of folder from where to load the service provider plugins
   */
  private async load(folders: string[]) {

    // If no folders provided, use default folder
    if (!folders || folders.length === 0) {
      folders = [`${__dirname}/plugins`];
    }

    try {

      // Register modules from folder
      const promises: Promise<void>[] = [];
      folders.forEach((folder) =>
        promises.push(this.registerModulesFromFolder(folder, this._serviceManager))
      );
      await Promise.all(promises);

      console.log(`Registered services:\n${JSON.stringify(this._serviceManager.serviceNames(), null, 2)}`);
      this.emit(EventName.AfterLoad, this._serviceManager);

    } catch (error) {
      console.log(`Failed to start DI framework:\n${error}`);
      this.emit(EventName.LoadError, error);
    }
  }

  /**
   * Looks up for module in provided folder recursively, loads the module and registers in service provider.
   * @param folder the folder from where to load files from.
   * @param serviceManager service provider manager where to inject the services.
   */
  private async registerModulesFromFolder(
    folder: string,
    serviceManager: ServiceProviderManager
  ): Promise<void> {

    // Load file system service
    const fileSystem = serviceManager.resolve(FileSystemService);
    const filePaths = await fileSystem.list(folder);

    // Add all modules to service provider
    return new Promise((resolve) => {
      filePaths
        .filter((path) => {
          const split = path.split('/');
          const fileName = split[split.length - 1];
          return !fileName.includes('service-provider') && !fileName.includes('file-system');
        })
        .forEach(async (path) => {
          console.log(`importing module '${path}'`);

          // Get name of the class
          const nodeModule = await import(path);
          const className = Object.keys(nodeModule)[0];

          // Check if module is excluded
          if (this._excludePlugins != null && this._excludePlugins.includes(className)) {
            console.log(`Excluded module '${className}'`);
            return;
          }

          try {
            // Try to register the module
            const provider: ServiceProvider = new nodeModule[className]();
            serviceManager.register(provider);
          } catch (error) {
            console.log(`Could not register provider '${className}':\n${error}`);
          }
        });

      resolve();
    });
  }
}
