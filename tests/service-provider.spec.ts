
import { ServiceProvider, ServiceProviderManager } from '../src/services';

class MockedProvider extends ServiceProvider {

}

class MockedSubclassProvider extends MockedProvider {

}

describe('service-provider', () => {
    let provider: ServiceProvider;
    let manager: ServiceProviderManager;

    beforeEach(() => {
        manager = new ServiceProviderManager();
    });

    it('should return correct service name', () => {
        provider = new MockedProvider();
        expect(provider.name()).toBe('MockedProvider');
    });

    it('should list all services it can provide correctly', () => {
        provider = new MockedSubclassProvider();
        expect(provider.provides()).toEqual([
            'MockedSubclassProvider',
            'MockedProvider'
        ]);
    });

    it('should not display ServiceProvider as provider class', () => {
        provider = new MockedSubclassProvider();
        expect(provider.provides()).not.toContain('ServiceProvider')
    });

    it('should add instance to ioc container if function invoked', () => {
        provider = new MockedSubclassProvider();
        expect(provider['serviceManager']).toBeNull();
        provider.addToIocContainer(manager);
        expect(provider['serviceManager']).toBe(manager);
    }); 
});